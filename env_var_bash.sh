
#Ogólnego użytku
alias undone='python /icm/hydra/home/kwolek/metadock/scripts/undone.py'
#alias obabel='/icm/hydra/home/kwolek/metadock/soft/openbabel/bin/obabel'
#alias timeout='/icm/hydra/home/kwolek/metadock/timeout'
#alias realpath='/icm/hydra/home/kwolek/metadock/realpath'

#Folder ze skryptami uruchamiającymi programy do dokowania
#apps=/icm/hydra/home/kwolek/metadock/apps
apps=/icm/hydra/home/kwolek/pdbbind/apps

#Folder ze skryptami pomocniczymi
scripts=/icm/hydra/home/kwolek/metadock/scripts

export set_env_var="source `pwd`/env_var_tcsh.sh"

