#!/bin/tcsh

#/icm/home/kwolek/soft/PipelinePilot/linux_bin/startserver
$set_env_var
set rec=$1
set lig=$2
set outd=$3
set work=$4
set clip=$5
set start=`pwd`





set lname=$lig:t:r
set rname=$rec:t:r
mkdir "$work"
set place="$work/$lname"
mkdir $place
cp $rec $lig  $place
cd $place



if ( -e ../$rname"_"prep.dsv ) then
cp ../$rname"_"prep.dsv ./
else
echo "czyszcze bialko"
echo "#\!/usr/bin/perl" > tmp.pl
echo "use strict;" >> tmp.pl
echo "use ProteinDiscoveryScript;" >> tmp.pl
echo "use ProtocolCommands;" >> tmp.pl
echo -n 'my $Receptor=' >> tmp.pl
echo "'$rname.pdb';" >> tmp.pl
echo -n 'my $folder=' >> tmp.pl
echo "'$place';" >> tmp.pl
echo 'my $server = "localhost:9943";' >> tmp.pl
echo 'my $document = DiscoveryScript::Open({' >> tmp.pl
echo '	Path      => $Receptor,' >> tmp.pl
echo "	ModelType => MdmModelType" >> tmp.pl
echo "});" >> tmp.pl
echo 'die "No molecular data loaded.\\n" unless ($document);' >> tmp.pl
echo 'my $parameters = Protocol::ParameterMap::Create();' >> tmp.pl
echo '$parameters->AddItem("Input Protein",$document->Molecules);' >> tmp.pl
echo '$parameters->AddItem("Keep Ligands",False);' >> tmp.pl
echo 'LaunchProtocol("Prepare Protein", $parameters, $server, 0, $folder);' >> tmp.pl


$ds tmp.pl


cp P*/Output/$rname'_'prep.dsv $place
cp $rname"_"prep.dsv ../
endif
cp ../${rname}_prep.dsv ./

set intmp='inscript.pl'
echo "robie inscript" 
echo "#\!/usr/bin/perl" > $intmp
echo "use strict;" >> $intmp
echo "use ProteinDiscoveryScript;" >> $intmp
echo "use ProtocolCommands;" >> $intmp
echo "use MdmDiscoveryScript;" >> $intmp
echo "use DSCommands;" >> $intmp
echo -n 'my $Receptor=' >> $intmp
echo "'${rname}_prep.dsv';" >> $intmp
echo -n 'my $Ligand=' >> $intmp
echo "'$lname.sdf';" >> $intmp

echo -n 'my $folder=' >> $intmp
echo "'$place';" >> $intmp
echo 'my $server = "localhost:9943";' >> $intmp



if ( "$clip" == 'False' ) then

echo 'my $x='"0;">> $intmp
echo 'my $y='"0;">> $intmp
echo 'my $z='"0;">> $intmp
echo 'my $R='"1;">> $intmp
cat /$scripts/script.cdocker.noclip >>$intmp
else
set grid=(`$scripts/protein_size.py $clip sphere 1.2`)
echo 'my $x='"${grid[1]};">> $intmp
echo 'my $y='"${grid[2]};">> $intmp
echo 'my $z='"${grid[3]};">> $intmp
echo 'my $R='"${grid[4]};">> $intmp
cat /$scripts/script.cdocker.clip >>$intmp
endif
echo "dokuje" 
$ds $intmp


foreach i (D*/Output/$lname.sd)
cat  $i >> cdocker"_"$rname"_"$lname.sd
end
python $scripts/convert_to_sdf.py cdocker"_"$rname"_"$lname.sd $outd
#done
cd $start
rm -r $place
#/icm/hydra/home/kwolek/metadock/soft/PipelinePilot/linux_bin/stopserver
