#!/usr/bin/python
#-*- coding: utf-8 -*-

import sys
import os.path

class Decoy:
	def __init__(self):
		self.header=str()
		self.lname=str()
		self.rname=str()
		self.aname=str()
		self.atoms=[]
		self.bonds=[]
		self.rest=[]
		self.garbage=str()
	def write(self,out):
		bonds=[]
		for i in self.bonds:
			if int(i[0].split()[1])<=len(self.atoms):
				bonds.append(i)
		if len(self.atoms)+len(bonds):
			
			
			print >>out,self.header
			print >>out, ""
			print >>out, ""
			nat=str(len(self.atoms))
			nat=(3-len(nat))*' '+nat
			nbd=str(len(bonds))
			nbd=(3-len(nbd))*' '+nbd
			print >>out, nat+nbd,self.garbage
			for i in self.atoms:
				for j in i:
					print >> out, j,
				print >> out, ""
			for i in bonds:
				for j in i:
					print >> out, j,
				print >> out, ""
			print >>out, "M  END"
			for i in self.rest:
				print >> out, i,
			print >> out, ""
			
			print >>out, "$$$$"
		
		
def read_decoy(dec):
	decoys=[]
	lines=dec.readlines()
	i=0
	new_decoy=True
	score=False
	nat=0
	nbd=0
	for line in lines:
		#print line[:-2]
		if new_decoy:
			decoys.append(Decoy())
			new_decoy=False
			i+=1
			k=0
			lname=line.split()[0]
			decoys[-1].header=line[:-1]
			la=0
			lb=0
			atoms=False
			bonds=False
		if "$$$$" in line:
			new_decoy=True
			
		
			
			
		if (not "$$$$" in line) and k>=4:
			if not bonds and not atoms:
				decoys[-1].rest.append(line)
			if "M  END" in line:
				bonds=False
				atoms=False
			if bonds:
				decoys[-1].bonds.append((line[:-1],""))
			if atoms:
				if '.' in line:
					decoys[-1].atoms.append((line[:-1],""))
				else:
					atoms=False
					bonds=True
					decoys[-1].bonds.append((line[:-1],""))
		if k==3:
			decoys[-1].garbage=" ".join(line.split()[2:])
			atoms=True
		k+=1
	
	return decoys

dec=open(sys.argv[1])
out=open(sys.argv[2],'w')

decoys=read_decoy(dec)
for decoy in decoys:
	decoy.write(out)

	

