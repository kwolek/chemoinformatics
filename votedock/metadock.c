#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>
#include <iostream>
#include <limits>

using namespace std;

 typedef struct{
   int    numa; // new number after sorting atoms, starts with 0
   float  coor[3];
   char   name; // coordinates of this atom
   int    type1; // hash of the local chemical structure
   int    type2; // hash of the local chemical structure
   char   numb; // number of bound atoms
   int    atoms[10]; // ids of bound atoms, max 10
   char   types[10]; // types of bound atoms, max 10
   } atom_t;
 typedef struct{
   char   *name; // name taken from first line of sdf
   float  *dists; // rmsds to other ligands
   atom_t *atoms; // atoms 
   atom_t **order; // order of atoms relative to first ligand
   } ligand_t;

 int numa=0; // number of atoms
 int numb=0; // number of bonds
 int numl=0; // number of ligands
 ligand_t *ligands=NULL; // ligands

void set_numbers(ligand_t* ligand)
{ int i,j;
  for(i=0;i<numa;i++){
    ligand->order[i]->numa=i;}
  for(i=0;i<numa;i++){
    for(j=0;j<ligand->order[i]->numb;j++){
      ligand->order[i]->atoms[j]=ligand->atoms[ligand->order[i]->atoms[j]].numa;}}
}

float dist(float* f1,float* f2)
{ return (f1[0]-f2[0])*(f1[0]-f2[0])+(f1[1]-f2[1])*(f1[1]-f2[1])+(f1[2]-f2[2])*(f1[2]-f2[2]);
}

/* start: generating all permutations */
void swap(int* Value,int i,int j) {
    int t;
    t=Value[i]; 
    Value[i]=Value[j]; 
    Value[j]=t;
}
float HeapPermute(float min_perm,float* data,int* Value,int N,int n) {
    int i;
    if (n <= 1) {
        float tmp_perm=0;
	for (i=0;i<N;i++){
           tmp_perm+=data[i*N+Value[i]];}
        if(tmp_perm<min_perm){
          min_perm=tmp_perm;}
	//for (int i=0;i<N;i++) printf("%2d",Value[i]);
	//printf("\n");
    }
    else {
	for (i=0;i<n;i++) {	    
	    min_perm=HeapPermute(min_perm,data,Value,N,n-1);
	if (n%2 == 1) 
	    swap(Value,0,n-1);
	else           
	    swap(Value,i,n-1);
	}
    }
    return min_perm;
}
/* end: generating all permutations */

float min_sum(int n,float* data,float* mind)
{ int c,d,m;
  float *min1=mind;
  float *min2=mind+n;

/**/
  if(n==2){
    if(data[0]+data[3]>data[1]+data[2]){ 
      return data[1]+data[2];}
    return data[0]+data[3];}
/**/
  if(n<=6){
    int Value[6];
    float min_perm=0;
    for (c=0;c<n;c++){
      Value[c]=c;
      min_perm+=data[c*n+c];}
    min_perm=HeapPermute(min_perm,data,Value,n,n);
    return(min_perm);}
  for(c=0;c<n;c++){
    min1[c]=data[c*n];
    min2[c]=data[c];}
  for(c=0;c<n;c++){
    for(d=0;d<n;d++){
      if(data[c*n+d]<min1[c]){ min1[c]=data[c*n+d];}
      if(data[c*n+d]<min2[d]){ min2[d]=data[c*n+d];}}}
  for(m=0,c=1;c<n*2;c++){
    if(mind[c]>mind[m]){ m=c;}}
  if(m>=n){
    d=m-n;
    for(c=0;c<n;c++){
      if(data[c*n+d]==min2[d]){ // c and d selected
        break;}}}
  else{
    c=m;
    for(d=0;d<n;d++){
      if(data[c*n+d]==min1[c]){ // c and d selected
        break;}}}
  assert(c<n);
  assert(d<n);
  /*if(n<=2){
    c=(c+1)%2;
    d=(d+1)%2;
    return mind[m]+data[c*n+d];}*/

/*
  if(n==2){
    int c1=(c+1)%2;
    int d1=(d+1)%2;
    float m1=mind[m]+data[c1*n+d1];
    if(m1>data[c *n+d1]+data[c1*n+d ]){
      fprintf(stdout,"%d %d %d %f %f %f\n",c,d,m,mind[m],m1,data[c *n+d1]+data[c1*n+d ]);
      { int c,d;
        for(c=0;c<n;c++){
          for(d=0;d<n;d++){
            fprintf(stdout,"%f ",data[c*n+d]);}
          fprintf(stdout,"\n");}}}}
*/

/*fprintf(stdout,"%d %d %d %f\n",c,d,m,mind[m]);
  { int c,d;
    for(c=0;c<n;c++){
      for(d=0;d<n;d++){
        fprintf(stdout,"%f ",data[c*n+d]);}
      fprintf(stdout,"\n");}
  }
*/

  if(c<n-1){
    int d;
    for(d=0;d<n;d++){
      data[c*n+d]=data[(n-1)*n+d];}}
  if(d<n-1){
    int c;
    for(c=0;c<n;c++){
      data[c*n+d]=data[c*n+(n-1)];}}

/*fprintf(stdout,"%d %d %d %f\n",c,d,m,mind[m]);
  { int c,d;
    for(c=0;c<n-1;c++){
      for(d=0;d<n-1;d++){
        fprintf(stdout,"%f ",data[c*n+d]);}
      fprintf(stdout,"\n");}
    fprintf(stdout,"\n");
  }
*/

  return mind[m]+min_sum(n-1,data,mind);
}

float min_dist(ligand_t* l1,ligand_t* l2)
{ float mdist=0.0;
  int a=0;
  for(;a<numa;a++){
    if(l1->order[a]->type2&0x40000000){
      break;}
    mdist+=dist(l1->order[a]->coor,l2->order[a]->coor);}
  for(;a<numa;){
    int b=a+1,n,c,d;
    float *data,*mind;
    for(;b<numa&&l1->order[a]->type2==l1->order[b]->type2;b++){}
    n=b-a;
    assert(n>1);
    data=(float*)malloc((n+2)*n*sizeof(float));
    mind=data+n*n;
    for(c=0;c<n;c++){
      for(d=0;d<n;d++){
        data[c*n+d]=dist(l1->order[a+c]->coor,l2->order[a+d]->coor);}}
    mdist+=min_sum(n,data,mind);
    free(data);
    a=b;}
  return sqrtf(mdist/(float)numa);
}

int check_data(ligand_t* l1,ligand_t* l2)
{ int i=0;
  for(;i<numa;i++){
	if(l1->order[i]->type2!='*'&&l2->order[i]->type2!='*'){
    if(l1->order[i]->type2!=l2->order[i]->type2){
      fprintf(stderr,"ERROR, mismatch between ligands\n");
      return 0;}}}
  return 1;
}

int bond_test(ligand_t* l1,ligand_t* l2,int a)
{ int a1,a2,b1,b2,nb;

  nb=l1->order[a]->numb;
  for(b1=0;b1<nb;b1++){
    a1=l1->order[a]->atoms[b1];
    if(a1>a){
      continue;}
    for(b2=0;;){
      a2=l2->order[a]->atoms[b2];
      if(a1==a2){
        break;}
      if(++b2>=nb){ // mismatch
        return 0;}}}
  return 1;
}

int check_bonds(ligand_t* l1,ligand_t* l2)
{ int a;
  for(a=0;a<numa;a++){
    // check only bonds of unique atoms
    if(l1->order[a]->type2&0x40000000){
      break;}
    if(!bond_test(l1,l2,a)){
      return 0;}} // fatal mismatch !
  return 1;
}

void print_data(ligand_t* ligand)
{ int i=0,j;
  int t=0,m=0;
  fprintf(stderr,"%s\n",ligand->name);
  for(;i<numa;i++){
    fprintf(stderr,"%8X %8X %10.3f%10.3f%10.3f %4d %c %d",
      ligand->order[i]->type1,
      ligand->order[i]->type2,
      ligand->order[i]->coor[0],
      ligand->order[i]->coor[1],
      ligand->order[i]->coor[2],
      ligand->order[i]->numa,
      ligand->order[i]->name,
      (int)ligand->order[i]->numb);
    if(t==ligand->order[i]->type2){
      m++;}
    t=ligand->order[i]->type2;
    for(j=0;j<(int)ligand->order[i]->numb;j++){
      fprintf(stderr,"  %c %d %d",
        ligand->order[ligand->order[i]->atoms[j]]->name,
        ligand->order[i]->types[j],
        ligand->order[i]->atoms[j]);}
    fprintf(stderr,"\n");}
  fprintf(stderr,"%d duplicates\n",m);
}

static int cmpi(const void *p1, const void *p2)
{ //fprintf(stdout,"%d %d\n",*(int*)p1,*(int*)p2);
  return (*(int*)p1-*(int*)p2);
}

static int cmpt(const void *p1, const void *p2)
{ //fprintf(stdout,"%8X %8X %c %c .\n",((atom_t**)p1)[0]->type2,((atom_t**)p2)[0]->type2,((atom_t**)p1)[0]->name,((atom_t**)p2)[0]->name);
  return (((atom_t**)p1)[0]->type2 - ((atom_t**)p2)[0]->type2);
}

int set_type1(ligand_t* ligand,int j)
{ int tn[20];
  int i=0;
  int hash=5381;
  hash=((hash<<5)+hash)+8*(int)ligand->atoms[j].name+(int)ligand->atoms[j].numb;
  for(;i<(int)ligand->atoms[j].numb;i++){
    tn[i]=8*(int)ligand->atoms[ligand->atoms[j].atoms[i]].name+(int)ligand->atoms[ligand->atoms[j].atoms[i]].numb;}
  qsort(tn,(size_t)ligand->atoms[j].numb,sizeof(int),cmpi);
  for(i=0;i<(int)ligand->atoms[j].numb;i++){
    hash=((hash<<5)+hash)+tn[i];}
  return abs(hash)&0x3FFFFFFF;
}

int set_type2(ligand_t* ligand,int j)
{ int tn[20];
  int i=0;
  int hash=(int)ligand->atoms[j].type2;
  for(;i<(int)ligand->atoms[j].numb;i++){
    tn[i]=ligand->atoms[ligand->atoms[j].atoms[i]].type1;}
  qsort(tn,(size_t)ligand->atoms[j].numb,sizeof(int),cmpi);
  for(i=0;i<(int)ligand->atoms[j].numb;i++){
    hash=((hash<<5)+hash)+tn[i];}
  return abs(hash)&0x3FFFFFFF;
}

int read_sdf(FILE* fin)
{ char line[1001];
  char name[1001];
  int num_a=0;
  int num_b=0;
  int heavy[1001];
  fill_n(heavy, 1001, -1);; // id after removing 'H';
  int i;
  int j;
  int t;
  ligand_t *ligand;
	//cerr<<"1 "<<line<<endl;
  ligands=(ligand_t*)realloc((void*)ligands,sizeof(ligand_t)*(numl+1));
  ligand=&ligands[numl];
  
  if(fgets(name,1000,fin)==NULL){ return -1;}
  i=strlen(name);
  name[i-1]='\0';
  ligand->name=(char*)malloc(i*sizeof(char));
  memcpy(ligand->name,name,i*sizeof(char));
  if(fgets(line,1000,fin)==NULL){ fprintf(stderr,"ERROR: (line1) (%s)\n",ligand->name); return -1;}
  if(fgets(line,1000,fin)==NULL){ fprintf(stderr,"ERROR: (line2) (%s)\n",ligand->name); return -1;}
  if(fgets(line,1000,fin)==NULL){ fprintf(stderr,"ERROR: (line3) (%s)\n",ligand->name); return -1;}
  //cerr<<"2 "<<line<<endl;
  line[6]='\0';
  num_b=atoi(line+3);
  line[3]='\0';
  num_a=atoi(line);
  //cout<<"3 "<<num_a<<" "<<num_b<<endl;
  ligand->atoms=(atom_t*)malloc(num_a*sizeof(atom_t));
  
  for(j=0,i=0;i<num_a;i++){
    if(fgets(line,1000,fin)==NULL){ fprintf(stderr,"ERROR: (line4) (%s)\n",ligand->name); return -1;}
    //////cerr<<i<<" "<<line<<endl;
    
    //char * tmp;
    //string tmp1;
    char at_type;
    //tmp1=string(line);
	//tmp = strtok (strdup(tmp1.c_str())," \t");
	//////cerr<<"tmp 1 "<<tmp<<endl;
	//for(int k=0;k<3;k++){
		//tmp = strtok(NULL, " \t");
		//////cerr<<tmp<<endl;
	//}
	//////cerr<<"line "<<line<<endl;
	////at_type=*tmp;
	////cerr<<"tmp 2 "<<tmp<<endl;
    if(line[31] != 'H'){
		//cerr<<"test1"<<endl;
      if(sscanf(line,"%f%f%f",&ligand->atoms[j].coor[0],&ligand->atoms[j].coor[1],&ligand->atoms[j].coor[2])!=3){
		 fprintf(stderr,"ERROR: (line5) (%s)\n",ligand->name); return -1;}
      ligand->atoms[j].name=line[31];
      ligand->atoms[j].numb=0;
      heavy[i]=j;
      j++;}
    else{
      heavy[i]=-1;}
      }
      ////cerr<<numa<<" "<<j<<" ";
  if(numa==0){
    numa=j;}
  num_a=j;
  for(j=0,i=0;i<num_b;i++){
    int t,a2,a1;
    if(fgets(line,1000,fin)==NULL){ fprintf(stderr,"ERROR: (line6) (%s)\n",ligand->name); return -1;}
    line[9]='\0';
    t=atoi(line+6);
    line[6]='\0';
    a2=atoi(line+3)-1;
    line[3]='\0';
    a1=atoi(line)-1;
    //cerr<<heavy[a1]<<" "<<heavy[a2]<<endl;
    if(heavy[a1]>=0 && heavy[a2]>=0){
      ligand->atoms[heavy[a1]].numb++;
      ligand->atoms[heavy[a1]].atoms[ligand->atoms[heavy[a1]].numb-1]=heavy[a2];
      ligand->atoms[heavy[a1]].types[ligand->atoms[heavy[a1]].numb-1]=(char)t;
      ligand->atoms[heavy[a2]].numb++;
      ligand->atoms[heavy[a2]].atoms[ligand->atoms[heavy[a2]].numb-1]=heavy[a1];
      ligand->atoms[heavy[a2]].types[ligand->atoms[heavy[a2]].numb-1]=(char)t;
      j++;}}
      ////cerr<<j<<" "<<numb<<endl;
  if(numb==0){
    numb=j;}
  num_b=j;
  if(fgets(line,1000,fin)==NULL){ fprintf(stderr,"ERROR: (line7) (%s)\n",ligand->name); return -1;}
  if(strncmp(line,"M  ",3)){ fprintf(stderr,"ERROR: (line8) (%s)\n",ligand->name); return -1;}
  for(;fgets(line,1000,fin)!=NULL;){
    if(!strncmp(line,"$$$$",4)){
      break;}}
  if(numa != num_a){
    fprintf(stderr,"ERROR: %d %d (numa), ligand skipped (%s)\n",numa,num_a,ligand->name);
    return 0;}
  if(numb != num_b){
    fprintf(stderr,"ERROR: %d %d (numb), ligand skipped (%s)\n",numb,num_b,ligand->name);
    return 0;}
  for(j=0;j<numa;j++){ // calculate hash values describing the 1-order local conformation of atoms
    ligand->atoms[j].type1=set_type1(ligand,j);
    ligand->atoms[j].type2=ligand->atoms[j].type1;}
  for(j=0;j<numa;j++){ // calculate hash values describing the 2-order local conformation of atoms
    ligand->atoms[j].type2=set_type2(ligand,j);}
  for(i=0;i<2;i++){
    for(j=0;j<numa;j++){
      ligand->atoms[j].type1=ligand->atoms[j].type2;}
    for(j=0;j<numa;j++){ // calculate hash values describing the i+2-order local conformation of atoms
      ligand->atoms[j].type2=set_type2(ligand,j);}}
  ligand->order=(atom_t**)malloc(numa*sizeof(atom_t*));
  for(j=0;j<numa;j++){ 
    ligand->order[j]=&ligand->atoms[j];}
  qsort(ligand->order,(size_t)numa,sizeof(atom_t*),cmpt);
  for(t=ligand->order[0]->type2,j=1;j<numa;j++){ 
    if(ligand->order[j]->type2==t){
      ligand->order[j]->type2=ligand->order[j]->type2|0x40000000; // marks atom as member of a symetry cluster
      ligand->order[j-1]->type2=ligand->order[j]->type2;}
    else{
      t=ligand->order[j]->type2;}}
  qsort(ligand->order,(size_t)numa,sizeof(atom_t*),cmpt);
  set_numbers(ligand);
  numl++;
  return 1;
}

int main(int argc,char** argv)
{ FILE *fin;
  int l,it=0;
  
	////cerr<<it<<endl;it++;
  if(argc<2){
    fprintf(stderr,"USAGE: %s file_in.sdf\n  %s tmp/2d3z/2d3z_decoys.sdf\n",*argv,*argv);
    exit(1);}
  fin=fopen(argv[1],"r");
  ////cerr<<it<<endl;it++;
  if(fin == NULL){
    fprintf(stderr,"ERROR: could not open input sdf file (%s), fatal\n",argv[1]);
    exit(-1);}
    ////cerr<<it<<endl;it++;
    
  for(;read_sdf(fin)>=0;);
  //for(;read_sdf(fin)>=0;){
	  //////cerr<<it<<endl;it++;
    //if(!numl){
      //continue;}
    //if(!check_data(ligands,&ligands[numl-1])){
      //fprintf(stderr,"ERROR: data check failed, fatal\n");
      //print_data(ligands);
      //print_data(&ligands[numl-1]);
      //numl--;
      //continue;}
    //if(!check_bonds(ligands,&ligands[numl-1])){
      //fprintf(stderr,"ERROR: sorting atoms failed, fatal\n");
      //print_data(ligands);
      //print_data(&ligands[numl-1]);
      //numl--;
      //continue;}
    //}
    ////cerr<<it<<endl;it++;
  fclose(fin);
  if(!numl){
    return -1;}
    
 
    int** infs = (int**)malloc(numl * sizeof(int*));
    for(int i = 0; i < numl; i++)
        infs[i] = (int*)malloc(numl * sizeof(int));
    for(int i = 0; i < numl ; i++)
		for(int j = i; j < numl ; j++){
			if(check_data(&ligands[i],&ligands[j]) && check_bonds(&ligands[i],&ligands[j]))
				infs[i][j]=0;
			else
				infs[i][j]=1;
			infs[i][j]=infs[j][i];
		}
		
    ////cerr<<it<<endl;it++;
  for(l=0;l<numl;l++){
    ligands[l].dists=(float*)malloc(numl*sizeof(float));}
  ligands[0].dists[0]=0.0;
  ////cerr<<it<<endl;it++;
  double inf = numeric_limits<double>::infinity();
  for(l=1;l<numl;l++){
    int m;
    ligands[l].dists[l]=0.0;
    for(m=0;m<l;m++){
		if(infs[l][m])
      ligands[l].dists[m]=min_dist(&ligands[l],&ligands[m]);
      else
      ligands[l].dists[m]=inf;
      ligands[m].dists[l]=ligands[l].dists[m];}}
      ////cerr<<it<<endl;it++;
  for(l=0;l<numl;l++){
    int m;
    fprintf(stdout,"%s\t",ligands[l].name);
    for(m=0;m<numl;m++){
      fprintf(stdout,"%.4f ",ligands[l].dists[m]);}
    fprintf(stdout,"\n");}
  return(0);
}

