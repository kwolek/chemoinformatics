#!/bin/bash

scores=$1
decoys=$2
number=`expr $3 + 1`
outd=$4


votedir="/media/D/Dropbox/docking/votedock"

for score in `ls $scores`; do
echo $score
out=$outd/`echo $score|cut -d '_' -f 1-2`.sdf
echo $score|cut -d '_' -f 1-2

for i in $(seq 2 $number); do
line=( `sed -n ${i}p $scores/$score` )
#echo $i ${line[0]} ${line[1]} ${line[2]} ${line[3]} ${line[4]}
file=$decoys/${line[1]}/`echo ${line[2]}|cut -d _ -f 1`/${line[0]}_${line[1]}_`echo ${line[2]}|cut -d _ -f 1`.sdf 
#echo $file
startToken="${line[0]} ${line[1]} ${line[2]} ${line[3]}"

python ~$votedir/cuttext.py $file $startToken > $out

done
done
