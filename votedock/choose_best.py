#!/usr/bin/python
#-*- coding: utf-8 -*-

import sys
import os
#from math import *
from numpy import *
import pylab as pl
import pickle
from sklearn.ensemble import RandomForestClassifier


[apps0,rfc]=pickle.load(open(sys.argv[1]))
filescr=open(sys.argv[2],'r')
#filermsd=open(sys.argv[3],'r')
fileout=open(sys.argv[3],'w')

data=filescr.readlines()
#data2=filermsd.readlines()
apps=data[0].split()[5:]
decoys=zeros([len(data)-1,len(data[0].split()[5:])])
#headers=zeros([len(data)-1,4],dtype="a20")
headers=[]
i=0
k=1
decs=[]
for line in data[1:]:
	headers.append(line.split()[:4])
	
	decoys[i]=array(line.split()[5:],dtype=float)
	decs.append(decoys[i])
	if headers[-1][0]=='clip':
		clipID=i
	i+=1
	k+=1
ndec=zeros([len(decoys),len(apps0)])

for a in xrange(len(apps)):
		i=apps0.index(apps[a])
		ndec[:,i]=decoys[:,a]
ndec2=[]
test2=zeros([len(headers),len(apps0)])
clip=ndec[clipID]
#clip=data[headers.index('clip')]
for i in xrange(len(headers)):
	ndec2.append([headers[i],ndec[i]])
	#print test2[i],ndec[i],clip
	#test2[i]=concatenate([clip,ndec[i]])
	test2[i]=clip-ndec[i]
def sorter(decoy1,decoy2):
	if decoy1[0]==decoy2[0]:
		return 0
	else:
		#test=concatenate([decoy1[1],decoy2[1]])
		test=decoy1[1]-decoy2[1]
		result=rfc.predict(test)
		if result==1:
			return 1
		else:
			return -1
points=zeros(len(ndec))
for i in xrange(1,len(ndec)):
	test3=ndec-roll(ndec,i,axis=0)
	points+=rfc.predict(test3)

#print array(headers)[:,0][points==min(points)]

#newdecoys=sorted(ndec2,cmp=sorter)
#print decoys
#print newdecoys

clip=True
tmpsc=points
tmphd=array(headers)
i=0
M=5
while i<M:
	t=min(tmpsc)==tmpsc	
	minscores=t
	k=sum(t)
	for dec in array(tmphd)[t]:
		print >> fileout,'\t'.join(dec)
	cut=min(tmpsc)!=tmpsc
	tmpsc=tmpsc[cut]
	tmphd=tmphd[cut]
	
	i+=k


#result=rfc.predict(test2)
#print sum(result),newdecoys[int(sum(result))][0][0]
#for ndec in newdecoys:
	#t='\t'.join(ndec[0])
	##print t
	#print >> fileout,t
