#!/bin/bash

start=`pwd`
votedir="/media/D/Dropbox/docking/votedock"
metadock="$votedir/metadock"
voting="python $votedir/voting.py"
score3d="python $votedir/3Dscore.py"
repair="python $votedir/repair_sdf.py"
dest=`realpath $2`
decoys=`realpath $1`
cd $decoys
choose_best=~/Dropbox/docking/votedock/choose_best.sh
mkdir -p $dest/bestconfs
mkdir -p $dest/3dscores
for p in $3*;do
	#mkdir -p $dest/$p
	cd $p

mkdir -p $dest/bestconfs/$p/votebest;
for i in $(seq 1 11);do 
mkdir -p $dest/3dscores/$p/vote$i;
cp $decoys/$p/*/*3dscore.vote$i $dest/3dscores/$p/vote$i;

mkdir -p $dest/bestconfs/$p/vote$i;
bash $choose_best $dest/3dscores/$p/vote$i $decoys 1 $dest/bestconfs/$p/vote$i;
if [ -e $dest/bestconfs/$p/vote$i/$p'_'$p.3dscore.vote$i.sdf ]; then
cat $dest/bestconfs/$p/vote$i/$p'_'$p.3dscore.vote$i.sdf > $dest/bestconfs/$p/votebest/$p'_'$p.3dscore.votebest.sdf
fi
#cat $dest/bestconfs/$p/vote$i/$p'_'$p.3dscore.vote$i.sdf> $decoys/$p/$p/conc2vote$i;
#cat $decoys/$p/$p/clip_$p'_'$p.sdf >> $decoys/$p/$p/conc2vote$i;
#$metadock $decoys/$p/$p/conc2vote$i > rmsdvote$i;
done


cd $decoys
done
