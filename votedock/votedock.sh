#!/bin/bash


###Skrypt analizuje wyniki, robi głosowanie, naprawia ewentualne błędy
### Wybór najlepszych konfiguracji przerzucony do skryptu choose_best.sh

start=`pwd`
votedir="/media/D/Dropbox/docking/votedock"
metadock="$votedir/metadock"
voting="python $votedir/voting.py"
score3d="python $votedir/3Dscore.py"
repair="python $votedir/repair_sdf.py"
dest=`realpath $2`
decoys=`realpath $1`
ddone=`realpath $3`
logs=`realpath $4`
cd $decoys
rm $logs/*
choose_best=$votedir/choose_best.sh
for p in $5*;do
	mkdir -p $ddone/$p
	cd $p
	for l in *;do
		#l=`basename $lp`
		pair=$p"_"$l
		cd $l
		
		echo $p $l
		log=$logs/$p-$l.test
		if [ ! -e "$pair.rmsd" ]; then
			
			conc=$pair-conc
			if [ -e "$conc" ]; then
				rm $conc
			fi
			for i in *.sdf;do
							
				cat $i >> $conc
			done
			$metadock $conc >$pair.rmsd 2> $log
		fi
		#cp $pair.rmsd $pair"_"rmsd
		
		if [ -s "$pair.rmsd" ]; then
		$voting $pair.rmsd
		for vote in *bestRMSD_vote*;do
		nr=${vote##*_}
		if [ -s $vote ]; then
		$score3d $vote > $pair.3dscore.$nr
		fi
		done 
		#$metascore $pair.rmsd $coeff > $pair.metascore
		#rm $pair.* $conc
		fi
		
		##sprawdzenie czy wszystko poszło dobrze i ewentualnie naprawa dla przyszłych pokoleń
		if [ -e $log ]; then
			
			if [ `cat $log|wc -l` == 0 ]; then
				cd $decoys/$p
				mv $l $ddone/$p
			else
			##zignorowanie nieistotnych będów
				ligskip=`grep -c 'ligand skipped' $log`
				ligmism=`grep -c 'mismatch between ligands' $log`
				#echo "expr $ligskip + $ligmism "
				if [ `expr $ligskip + $ligmism` == `cat $log|wc -l` ];then	
					cd $decoys/$p
					mv $l $ddone/$p
				else
					rm $decoys/$p/$l/$pair.* $decoys/$p/$l/$conc
					##naprawa błędów związanych z użycie starych wersji skryptów
					if [ `grep -c 'line8' $log` != 0 ];then
						to_rep=$decoys/$p/$l/`grep 'line8' $log|cut -d ')' -f 2|cut -d '(' -f 2|cut -f1-3|sed 's/\t/_/g'`.sdf
						echo $to_rep
						python $votedir/repair_sdf.py $to_rep tmp
						mv tmp $to_rep;
					fi
					if [ `grep -c 'line5' $log` != 0 ];then
						to_rep=$decoys/$p/$l/`grep 'line5' $log|cut -d ')' -f 2|cut -d '(' -f 2|cut -f1-3|sed 's/\t/_/g'`.sdf
						echo $to_rep
						python $votedir/repair_line5.py $to_rep tmp
						mv tmp $to_rep;
					fi
				
				fi
			fi
		#if [ ! `grep -c 'data check failed' $log` == 0 ]; then 
		### dopisać sprawdzenie czy to aby na pewno autodocki/coś co poprawi autodocki
		#if [ `grep -c 'autodock' $log` != 0 ];then
		#cd $decoys/$p/$l
		#rm autodock*
		#cd $decoys/$p
		#fi
		#if [ `grep -c 'vina' $log` != 0 ];then
		#cd $decoys/$p/$l
		#rm vina*
		#cd $decoys/$p
		#fi
		#fi
		else
		rm $pair.* $conc
		
		fi
		
		cd $decoys/$p
	done 
	


#for i in $(seq 1 11);do 
#mkdir -p $dest/3dscores/$p/vote$i;
#cp $decoys/$p/*/*3dscore.vote$i $dest/3dscores/$p/vote$i;
#done

#mkdir -p $dest/bestconfs/$p/votebest;
#for i in $(seq 1 11);do 
#mkdir -p $dest/bestconfs/$p/vote$i;
#bash $choose_best $dest/3dscores/$p/vote$i $decoys 1 $dest/bestconfs/$p/vote$i;
#if [ -e $dest/bestconfs/$p/vote$i/$p'_'$p.3dscore.vote$i.sdf ]; then
#cat $dest/bestconfs/$p/vote$i/$p'_'$p.3dscore.vote$i.sdf > $dest/bestconfs/$p/votebest/$p'_'$p.3dscore.votebest.sdf
#fi
#cat $dest/bestconfs/$p/vote$i/$p'_'$p.3dscore.vote$i.sdf> $decoys/$p/$p/conc2vote$i;
#cat $decoys/$p/$p/clip_$p'_'$p.sdf >> $decoys/$p/$p/conc2vote$i;
#$metadock $decoys/$p/$p/conc2vote$i > rmsdvote$i;
#done


cd $decoys
if [ `ls $p|wc -l` == 0 ];then
rm -r $p
fi
done
cd $start





