#!/bin/bash


#Ogólnego użytku
set scripts=/home/kwolek/metadock/scripts
echo $scripts
alias obabel /home/kwolek/metadock/soft/openbabel/bin/obabel
alias timeout /home/kwolek/metadock/timeout 
alias realpath /icm/hydra/home/kwolek/metadock/realpath

#Autodock4 i Vina
set ad4binpath=/opt/metadock/soft/autodock4/i86_64Linux2
alias autodock4 /home/kwolek/metadock/soft/autodock4/autodock/build/autodock4
alias autogrid4 /home/kwolek/metadock/soft/autodock4/autogrid/build/autogrid4


set adt=/home/kwolek/metadock/soft/mgltools/MGLToolsPckgs/AutoDockTools/Utilities24
setenv PYTHONPATH /home/kwolek/metadock/soft/mgltools/MGLToolsPckgs

alias vina /home/kwolek/metadock/soft/vina/vina

#Dock6

set dock6bindir=/home/kwolek/metadock/soft/dock6/bin
set dms=/home/kwolek/metadock/soft/dms


#eHiTS

alias ehits /home/kwolek/metadock/soft/ehits/ehits.sh


#GemDock

set gem_bin_dir=/icm/hydra/home/kwolek/metadock/soft/iGEMDOCK/iGEMDOCKv2.1-centos/bin
alias gemdock $gem_bin_dir/mod_ga
alias mod_cav $gem_bin_dir/mod_cav


#Plants

alias plants /home/kwolek/metadock/soft/plants/PLANTS1.2_64bit
alias spores /home/kwolek/metadock/soft/plants/SPORES_64bit

#Glamdock
set chill_dir=/home/kwolek/metadock/soft/chill_complete_0.5_MoDeST
alias glamdock "scripts/chillrun.sh -e dockingbenchmark spring-conf/docking/glamdock_screening.rb "

#Discovery Studio
set ds='/home/kwolek/metadock/soft/Accelrys/DiscoveryStudio31/bin/perl.sh'

