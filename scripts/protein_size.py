#!/usr/bin/python
#-*- coding: utf-8 -*-


##Plik zwracający wielkość białka/miejsca wiążącego w odpowiednim formacie
##mode:
###vina - współrzędne środka i długości krawędzi boxa w obrębie którego odbędzie się dokowanie (pobiera dodatkowo informację o marginie dla każdej współrzednej)
### autodock4 - współrzędne środka i ilość pól w danycm wymiarze (dodatkowo pobiera informację o marginie i grid_space)
### sphere - współrzedne środka i promień sfery obejmującej (wymaga dodatkowej informacji o przeskalowaniu promienia)
import sys
from math import sqrt
clip=sys.argv[1]
prot=open(sys.argv[1],'r').readlines()
mode=sys.argv[2]

x=[]
y=[]
z=[]
if(clip[-3:]=='pdb'):
	for line in prot:
		t=line.split()
		if len(t):
			if t[0]=='ATOM' or t[0]=='HETATM':
				x.append(float(line[30:38]))
				y.append(float(line[38:46]))
				z.append(float(line[46:54]))
elif(clip[-3:]=='sdf'):		
	nat=0
	nbd=0
	k=0
	for line in prot:
		t=line.split()
		
		if k==3:
			nat=int(line[:3])+4
			nbd=int(line[3:6])+nat
		if k<nat and k>3:
			x.append(float(t[0]))
			y.append(float(t[1]))
			z.append(float(t[2]))
		k+=1

xmin=min(x)
xmax=max(x)
ymin=min(y)
ymax=max(y)
zmin=min(z)
zmax=max(z)

if mode=='vina':
	dx=float(sys.argv[3])
	dy=float(sys.argv[4])
	dz=float(sys.argv[5])
	print "--center_x", (xmax+xmin)/2,"--center_y",(ymax+ymin)/2,"--center_z",(zmax+zmin)/2,"--size_x",(xmax-xmin)+dx,"--size_y",(ymax-ymin)+dy,"--size_z",(zmax-zmin)+dz
elif mode=='galaxydock':
	width=float(sys.argv[3])
	add=float(sys.argv[4])
	print 'grid_box_cntr',(xmax+xmin)/2,(ymax+ymin)/2,(zmax+zmin)/2,'\n'
	print 'grid_n_elem',int(round((xmax-xmin+add)/width)),int(round((ymax-ymin+add)/width)),int(round((zmax-zmin+add)/width)),'\n'
	print 'grid_width',width,'\n'
elif mode=='sphere':
	scale=float(sys.argv[3])	
	print (xmax+xmin)/2,(ymax+ymin)/2,(zmax+zmin)/2,scale*sqrt(((xmax-xmin)*(xmax-xmin)+(ymax-ymin)*(ymax-ymin)+(zmax-zmin)*(zmax-zmin)))/2
	
