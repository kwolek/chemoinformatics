#!/usr/bin/python
#-*- coding: utf-8 -*-


##Skrypt dodający odpowiednie headery dla wyników z surflexa
import sys
import os.path

sdf=open(sys.argv[1],'r').readlines()
scr=open(sys.argv[2],'r').readlines()
rname=sys.argv[3]
outdir=sys.argv[4]

lines=[]
ligs=[]
new=True
i=1

for line in sdf:	
	if new:
		t=scr[i].split('|')
		lname='-'.join(t[0].split('_')[:-1])
		number=t[0].split('_')[-1]
		score=t[1]
		if lname not in ligs:
			ligs.append(lname)
			lines.append([])
			
		k=ligs.index(lname)
		lines[k].append("surflex "+rname+" "+lname+" "+number+" "+score)
		new=False
	elif "M  END" in line:
		lines[k].append(line[:-1])
		lines[k].append("> <LOG>")
		lines[k].append(scr[i][:-1])
	elif "$$$$" in line:
		i+=1
		new=True
		lines[k].append(line[:-1])
	else:
		lines[k].append(line[:-1])


for i in xrange(len(ligs)):
	if ligs[i]!='****':
		directory=outdir+'/'+ligs[i]
		if not os.path.exists(directory):
			os.makedirs(directory)
		out=open(directory+"/surflex_"+rname+"_"+ligs[i]+".sdf",'w+')
		for line in lines[i]:
			print >> out,line
	
