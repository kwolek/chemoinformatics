#!/usr/bin/python


##Skrypt tworzący pliki inputowe dla glide zarówno dla tworzenia gridu jak i dokowania
import sys

#pobranie zmiennych
out=open(sys.argv[1],'w+')
bialko=sys.argv[6]
grid_file=sys.argv[9]
clip=sys.argv[3]
entry=sys.argv[7]
grid=sys.argv[4]
outbox=sys.argv[5]
all_ligs=sys.argv[8]
recfile=sys.argv[10]


x=[]
y=[]
z=[]

#Określenie miejsca wiążącego
if(clip!="False"):
	lig_file=open(clip,'r').readlines()
	end=int(lig_file[3].split()[0])+4

	print len(x)
	k=0
	for line in lig_file[4:end]:
		#print line[:-1]
		x.append(float(line.split()[0]))
		y.append(float(line.split()[1]))
		z.append(float(line.split()[2]))
	
	#grid=str(int(max([max(z)-min(z),max(y)-min(y),max(x)-min(x)])*2))
elif(clip[-3:]=='pdb'):
	prot=open(recfile,'r').readlines()
	for line in prot:
		t=line.split()
		if len(t):
			if t[0]=='ATOM':
				x.append(float(line[30:38]))
				y.append(float(line[38:46]))
				z.append(float(line[46:54]))
elif(clip[-3:]=='sdf'):
	prot=open(recfile,'r').readlines()		
	nat=0
	nbd=0
	k=0
	for line in prot:
		t=line.split()
		
		if k==3:
			nat=int(line.split()[0])+4
			nbd=int(line.split()[1])+nat
		if k<nat and k>3:
			x.append(float(t[0]))
			y.append(float(t[1]))
			z.append(float(t[2]))
		k+=1
#xmin=min(x)
#xmax=max(x)
#ymin=min(y)
#ymax=max(y)
#zmin=min(z)
#zmax=max(z)
	
sredniax=str((max(x)+min(x))/2)
sredniay=str((max(y)+min(y))/2)
sredniaz=str((max(z)+min(z))/2)
dx=str(max(x)-min(x)+float(outbox))
dy=str(max(y)-min(y)+float(outbox))
dz=str(max(z)-min(z)+float(outbox))
#dx=outbox
#dy=outbox
#dz=outbox


##Przygotowanie odpowiednich plików
##Opis poszczególnych opcji znajduje się w manualu glide
print >> out, "REC_MAECHARGES YES"
print >> out, "INNERBOX "+grid+", "+grid+", "+ grid
print >> out, "ACTXRANGE "+dx
print >> out, "ACTYRANGE "+dy
print >> out, "ACTZRANGE "+dz
print >> out, "GRID_CENTER "+ sredniax+", "+sredniay+", "+sredniaz
print >> out, "OUTERBOX "+dx+", "+dy+", "+dz
print >> out, "ENTRYTITLE "+entry
print >> out, "GRIDFILE "+grid_file
print >> out, "RECEP_FILE "+bialko
out.close()
out=open(sys.argv[2],'w+')
print >> out, "DOCKING_METHOD confgen"
print >> out, "PRECISION SP"
print >> out, "WRITEREPT YES"
print >> out, "FORCEPLANAR YES"
print >> out, "USECOMPMAE YES"
print >> out, "POSTDOCK_NPOSE 10"
print >> out, "POSES_PER_LIG 10"
print >> out, "RINGCONFCUT 2.500000"
print >> out, "GRIDFILE "+grid_file
print >> out, "LIGANDFILE "+all_ligs
print >> out, "LIGFORMAT sd"
print >> out, "POSE_OUTTYPE ligandlib_sd"
