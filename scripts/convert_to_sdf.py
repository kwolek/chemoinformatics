#!/usr/bin/python
#-*- coding: utf-8 -*-


##Mylna nazwa.
##Plik nadaje plikom wynikowym z kilku programów odpowiednie headery, dzieli na oddzielne pliki dla każdego ligandu i zapisuje w odpowiednim miejscu
##Kilka programów potrzebuje pliku sdf dla ligandu mającego dobrze zdefiniowane wiązania(swoje informację albo tracą albo zniekstałcają)
##Do poprawy: niekanoniczny sdf - źle zapisywana ilość atomów i wiązań
import sys
import os.path

def shorter(a,b):
	if len(a)<len(b):
		return a
	else:
		return b

class Decoy:
	def __init__(self):
		self.header=str()
		self.lname=str()
		self.rname=str()
		self.aname=str()
		self.atoms=[]
		self.bonds=[]
		self.rest=[]
		self.garbage=str()
	def write(self,out):
		bonds=[]
		for i in self.bonds:
			if int(i[0].split()[1])<=len(self.atoms):
				bonds.append(i)
		print >>out,self.header
		print >>out, ""
		print >>out, ""
		nat=str(len(self.atoms))
		nat=(3-len(nat))*' '+nat
		nbd=str(len(bonds))
		nbd=(3-len(nbd))*' '+nbd
		print >>out, nat+nbd,self.garbage
		for i in self.atoms:
			for j in i:
				print >> out, j,
			print >> out, ""
		for i in bonds:
			for j in i:
				print >> out, j,
			print >> out, ""
		print >>out, "M  END"
		for i in self.rest:
			print >> out, i,
		print >> out, ""
		
		print >>out, "$$$$"

def make_sdf(dest,decoys):
	##Funkcja zapisuje sdf do odpowiednich folderów
	out={}
	ligs=[]
	for decoy in decoys:
		t=decoy.lname
		if t in ligs:
			decoy.write(out[t])
		else:
			ligs.append(t)
			directory=dest+'/'+decoy.lname
			if not os.path.exists(directory):
			    os.makedirs(directory)
			out[t]=open(dest+'/'+decoy.lname+"/"+decoy.aname+'_'+decoy.rname+'_'+decoy.lname+'.sdf','w+')
			decoy.write(out[t])
	#out.close()
	
def read_vina_decoy(vina_decoy,rname,aname,input_molecule):
	molecule=read_ds_decoy(input_molecule,rname,aname)
	
	decoys=[]
	dec=open(vina_decoy)
	lines=dec.readlines()
	i=0
	new_decoy=True
	score=False
	nat=0
	nbd=0
	for line in lines:
		t=line.split()
		
		if new_decoy:
			decoys.append(Decoy())
			decoys[-1].bonds=molecule[0].bonds
			new_decoy=False
			i+=1
			k=0
			lname=line.split()[0]
		if "$$$$" in line:
			new_decoy=True
		if (not "$$$$" in line)  and (not "M  END" in line) and k>=nbd and k>=4:
			decoys[-1].rest.append(line[:-1])
		if "VINA RESULT" in line:
			decoys[-1].header=aname+"\t"+rname+"\t"+lname+"\t"+str(i)+"\t"+t[2]
			decoys[-1].lname=lname
			decoys[-1].rname=rname
			decoys[-1].aname=aname
		if k==3:
			nat=int(line[:3])+4
			nbd=int(line[3:6])+nat
			decoys[-1].garbage=" ".join(line.split()[2:])
		if k<nat and k>3:
			decoys[-1].atoms.append((line[:-1],""))
		k+=1
			
	return decoys
def read_autodock4_decoy(vina_decoy,rname,aname,input_molecule):
	molecule=read_ds_decoy(input_molecule,rname,aname)
	
	decoys=[]
	dec=open(vina_decoy)
	lines=dec.readlines()
	i=0
	new_decoy=True
	score=False
	nat=0
	nbd=0
	for line in lines:
		t=line.split()
		
		if new_decoy:
			decoys.append(Decoy())
			decoys[-1].bonds=molecule[0].bonds
			new_decoy=False
			i+=1
			k=0
			lname=line.split('.')[0]
		if "$$$$" in line:
			new_decoy=True
		if (not "$$$$" in line)  and (not "M  END" in line) and k>=nbd and k>=4:
			decoys[-1].rest.append(line[:-1])
		if "Estimated Free Energy of Binding" in line:
			decoys[-1].header=aname+"\t"+rname+"\t"+lname+"\t"+str(i)+"\t"+t[6]
			decoys[-1].lname=lname
			decoys[-1].rname=rname
			decoys[-1].aname=aname
		if k==3:
			nat=int(line[:3])+4
			nbd=int(line[3:6])+nat
			decoys[-1].garbage=" ".join(line.split()[2:])
		if k<nat and k>3:
			decoys[-1].atoms.append((line[:-1],""))
		k+=1
	return decoys
def read_ehits_decoy(decoy,rname,aname):
	decoys=[]
	dec=open(decoy)
	lines=dec.readlines()
	i=0
	new_decoy=True
	score=False
	nat=0
	nbd=0
	for line in lines:
		#print line[:-2]
		if new_decoy:
			decoys.append(Decoy())
			new_decoy=False
			i+=1
			k=0
			lname=line.split()[0]
		if "$$$$" in line:
			new_decoy=True
			
		if k==2:
			tmp=line.split()[3]
			
			decoys[-1].header=aname+"\t"+rname+"\t"+lname+"\t"+str(i)+"\t"+tmp
			decoys[-1].lname=lname
			decoys[-1].rname=rname
			decoys[-1].aname=aname
		if (not "$$$$" in line)  and (not "M  END" in line) and k>=nbd and k>=4:
			decoys[-1].rest.append(line)
		if k==3:
			nat=int(line[:3])+4
			nbd=int(line[3:6])+nat
			decoys[-1].garbage=" ".join(line.split()[2:])
		if k<nat and k>3:
			decoys[-1].atoms.append((line[:-1],""))
		if k<nbd and k>=nat:
			decoys[-1].bonds.append((line[:-1],""))
		k+=1
	return decoys
def read_ds_decoy(decoy,rname,aname):
	decoys=[]
	dec=open(decoy)
	lines=dec.readlines()
	i=0
	new_decoy=True
	score=False
	nat=0
	nbd=0
	for line in lines:
		#print line[:-2]
		if new_decoy:
			decoys.append(Decoy())
			new_decoy=False
			i+=1
			k=0
			lname=line.split()[0]
			decoys[-1].lname=lname
			decoys[-1].rname=rname
			decoys[-1].aname=aname
		if "$$$$" in line:
			new_decoy=True
			
		if score:
			#tmp=line
			decoys[-1].header=aname+"\t"+rname+"\t"+lname+"\t"+str(i)+"\t"+line[:-2]

			score=False
		if "LibDockScore" in line or "DOCK_SCORE" in line or "CDOCKER_ENERGY" in line:
			score=True
		if (not "$$$$" in line)  and (not "M  END" in line) and k>=nbd and k>=4:
			decoys[-1].rest.append(line[:-1])
		if k==3:
			nat=int(line[:3])+4
			nbd=int(line[3:6])+nat
			decoys[-1].garbage=" ".join(line.split()[2:])
		if k<nat and k>3:
			decoys[-1].atoms.append((line[:-2],""))
		if k<nbd and k>=nat:
			decoys[-1].bonds.append((line[:-2],""))
		k+=1
	return decoys
def read_glide_decoy(decoy,rname,aname):
	decoys=[]
	dec=open(decoy)
	lines=dec.readlines()
	i=0
	new_decoy=True
	score=False
	nat=0
	nbd=0
	for line in lines:
		#print line[:-2]
		if new_decoy:
			decoys.append(Decoy()) 
			new_decoy=False
			i+=1
			k=0
			lname=line.split()[0]
		if "$$$$" in line:
			new_decoy=True
			
		if score:
			#tmp=line
			decoys[-1].header=aname+"\t"+rname+"\t"+lname+"\t"+str(i)+"\t"+line[:-2]
			decoys[-1].lname=lname
			decoys[-1].rname=rname
			decoys[-1].aname=aname
			score=False
		if "glide_gscore" in line:
			score=True
		if (not "$$$$" in line)  and (not "M  END" in line) and k>=nbd and k>=4:
			decoys[-1].rest.append(line)
		if k==3:
			nat=int(line[:3])+4
			nbd=int(line[3:6])+nat
			decoys[-1].garbage=" ".join(line.split()[2:])
		if k<nat and k>3:
			decoys[-1].atoms.append((line[:-2],""))
		if k<nbd and k>=nat:
			decoys[-1].bonds.append((line[:-2],""))
		k+=1
	return decoys

def read_igemdock_decoy(decoy,rname,aname,input_molecule):
	molecule=read_ds_decoy(input_molecule,rname,aname)
	
	decoys=[]
	dec=open(decoy)
	lines=dec.readlines()
	i=0
	new_decoy=True
	score=False
	nat=0
	
	for line in lines:
		t=line.split()
		
		if new_decoy:
			decoys.append(Decoy())									
			new_decoy=False
			i+=1
			k=0
			lname=line.split('.')[0].split('-')[-2]
			for mol in xrange(len(molecule)):
				print molecule[mol].lname,lname
				if molecule[mol].lname==lname:
					
					m=mol
					decoys[-1].bonds=molecule[m].bonds
					break
		if "$$$$" in line:
			new_decoy=True
			
		if score:
			#tmp=line
			decoys[-1].header=aname+"\t"+rname+"\t"+lname+"\t"+str(i)+"\t"+t[0]
			decoys[-1].lname=lname
			decoys[-1].rname=rname
			decoys[-1].aname=aname
			score=False
		if "FitnessValue" in line:
			score=True
		if k==3:
			nat=int(line[:3])+4
			nbd=int(line[3:6])+nat
			decoys[-1].garbage=" ".join(line.split()[2:])
		if k<nat and k>3:
			decoys[-1].atoms.append((line[:-1],""))
		k+=1
			
	return decoys

t=sys.argv[1]
g=t.split("/")	
g=g[-1].split('_')
good=True
#print g
if g[0]=='vina':
	decoys=read_vina_decoy(t,g[1],g[0],sys.argv[3])
elif g[0]=='autodock4':
	decoys=read_autodock4_decoy(t,g[1],g[0],sys.argv[3])
elif g[0]=='cdocker':
	decoys=read_ds_decoy(t,g[1],g[0])
elif g[0]=='ligandfit':
	decoys=read_ds_decoy(t,g[1],g[0])
elif g[0]=='libdock':
	decoys=read_ds_decoy(t,g[1],g[0])
elif g[0]=='glide':
	decoys=read_glide_decoy(t,g[1],g[0])
elif g[0]=='ehits':
	decoys=read_ehits_decoy(t,g[1],g[0])
elif g[0]=='gemdock':
	decoys=read_igemdock_decoy(t,g[1],g[0],sys.argv[3])
else:
	print >>sys.stderr, "Nieznany program/format zapisu"
	good=False
	
if good:
	#print len(decoys)
	make_sdf(sys.argv[2],decoys)
	

