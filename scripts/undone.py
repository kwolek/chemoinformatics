#!/usr/bin/python
#-*- coding: utf-8 -*-


#Tworzy plik sdf z niepowtarzającymi się ligandami oraz bez ligandów które zostały już zadokowanie
import sys

sdf=open(sys.argv[1],'r').readlines()
done=open(sys.argv[2],'r').readlines()
out=open(sys.argv[3],'w+')
aname=sys.argv[4]

lines=[]
ligs=[]

for line in done:
	t=line.split('/')[-1].split('.')[0].split('_')[-1]
	if t not in ligs:
		ligs.append(t)
		#print t
new=True
i=1
write=True
k=-1
remove_prev=False
for line in sdf:
	t=line	
	if new:
		if remove_prev:
			lines.pop()
			remove_prev=False
			k-=1
		lname=line.split()[0]
		
		if lname in ligs:
			write=False
		else:
			write=True
			ligs.append(lname)
			lines.append([])		
			k+=1
		odd=(aname=='surflex' or aname=='glide')
		if len(lname)==1 and odd:
			lname+='_'
			t=lname+'\n'
		new=False
	#print t
	
		
	if "$$$$" in line:
		
		new=True
		
	if write:
		if aname=='surflex':
			if '*' in line:
				remove_prev=True
		#print len(lines),k
		lines[k].append(t[:-1])
		
print len(lines)
for ligand in lines:
	for line in ligand:
		print >> out,line

