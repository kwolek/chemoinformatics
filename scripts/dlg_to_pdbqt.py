#!/usr/bin/python
#-*- coding: utf-8 -*-

#Skrypt wyciąga informację o zadokowanych strukturach z pliku wyjściowego autodocka

import sys

dlg=open(sys.argv[1],'r').readlines()
pdbqt=open(sys.argv[2],'w+')
read=False
for line in dlg:
	if line[:5]=='MODEL':
		read=True
	if read:
		pdbqt.write(line)
	if line[:6]=='ENDMDL':
		read=False
		
