#!/bin/bash

shopt -s expand_aliases
source env_var_bash.sh
echo $apps
decoys=`pwd`/decoys
works=`pwd`/work
clips=False
logs=`pwd`/logs
SLURM_DATA=~/metadock/SLURM_DATA_DEFAULT
while [ $# -gt 0 ]
do
    case "$1" in
		-r)	recs=`realpath $2`;echo $2; shift;;
		-l)	ligs=`realpath $2`;echo $2; shift;;
		-ld)	ligs_dir=`realpath $2`;echo $2; shift;;
		-c)	mkdir -p $2;clips=`realpath $2`;echo $2; shift;;
		-w)	mkdir -p $2;works=`realpath $2`;echo $2; shift;;
		-o)	mkdir -p $2;decoys=`realpath $2`;echo $2; shift;;
		-e)	mkdir -p $2;logs=`realpath $2`;echo $2; shift;;
		-p)	SLURM_DATA=`realpath $2`;echo $2; shift;;
		-N) name="-J $2";echo $2; shift;;
		*)
                echo >&2 \
		"usage: $0 -r PROTEIN_DIR [-l LIGANDS_FILE|-ld LIGANDS_DIR] [-p PBS_DATA] [-c CLIPS_DIR] [-w WORK_DIR]  [-o OUTPUT_DIR]  [-e LOGS_DIR] [-N JOB_NAME]"
		exit 1;;
#	*)	break;;		# terminate while loop
    esac
    shift
done

if [[  $recs  &&  ( $ligs  ||  $ligs_dir ) ]] ;then

start=`pwd`
if [ ! -d "$decoys" ];then
mkdir $decoys
fi
if [ ! -d "$logs" ];then
mkdir $logs
fi
if [ -e $logs/to.do ]; then
rm $logs/to.do
fi
for rec in `ls $recs/*.pdb`;do
tmp=`basename $rec`
rname=${tmp%.*}
if [ ! -d "$decoys/$rname" ];then
mkdir $decoys/$rname
fi
for app in `ls $apps/*.sh`;do


tmp=`basename $app`
aname=${tmp%.*}

work=$works/$aname/$rname
mkdir -p $work

#Zebranie informacji co zostało już zrobione
ls $decoys/$rname/*/$aname'_'$rname'_'*.sdf 2> err 1> ldone

tmpligs=tmpligs.sdf
raligs=$works/$aname-$rname-ligs.sdf

if [ -e $ligs_dir/$rname.sdf ]; then
pligs=$ligs_dir/$rname.sdf
fi


#Przygotowanie zestawu ligandów do przerobienia
obabel $ligs $pligs -O$tmpligs -h
undone $tmpligs ldone $raligs $aname
#if [ -e $clips/$rname.sdf ] || [ -e $clips/$rname.pdb ]; then
#if [ ! -e $clips/$rname.sdf ];then
#$obabel $clips/$rname.pdb -O$clips/$rname.sdf
#fi
#clip=$clips/$rname.sdf
#else
#clip='False'
#fi
if [ -e $clips/$rname.sdf ]; then
clip=$clips/$rname.sdf
else
clip='False'
fi


if [ -s $raligs ]; then

dock=$logs/dock.sh

cat $SLURM_DATA > $dock
echo "setenv set_env_var '$set_env_var'">>$dock
echo "time tcsh $app $rec $raligs $decoys/$rname $work $clip">>$dock
echo "$aname	$rname"

#Dodanie zadania do kolejki
sbatch -o $logs/$aname'_'$rname.out  $name $dock
#sleep 5
fi
done
done
rm err ldone

else
echo "Nie podano zbioru białek i/lub ligandów"
fi

