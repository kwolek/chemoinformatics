#!/bin/tcsh

##Deklaracja podstawowych zmiennych
$set_env_var
set rec=$1
set ligs=$2
set outd=$3
set work=$4
set clip=$5

set aname=dock6rigid

set lname=$ligs:t:r
set rname=$rec:t:r
set cname=$clip:t:r
mkdir -p $work
#cp *.in $work
cd $work

set rclean=$rname'_'clean.pdb
obabel $ligs -O$lname.mol2

set cname=clip
if ( "$clip" == 'False' ) then
obabel $rec -O$cname.mol2 -h
set space=0.75
else
obabel $clip -O$cname.mol2 -h
set space=0.5
endif


##Przygotowanie miejsca wiążącego
if ( ! -s selected_spheres.sph )  then
if ( ! -s rec.sph ) then
if ( -e rec.sph ) then
rm rec.sph
endif
python $scripts/clean_pdb.py $rec tmp.pdb
obabel tmp.pdb -O$rclean -h


cp $dms/lib/radii.proto radii
$dms/dms $rclean -o rec.ms


# Construct sphere set in active site
echo "rec.ms" >INSPH
echo "R" >>INSPH
echo "X" >>INSPH
echo "0.0" >>INSPH
echo "4.0" >>INSPH
echo "1.4" >>INSPH
echo "rec.sph" >>INSPH

$dock6bindir/sphgen_cpp
#rm OUTSPH temp1.ms temp3.atc temp2.sph INSPH
endif



# Select spheres within 10 Ang of ligand 

$dock6bindir/sphere_selector rec.sph $cname.mol2 7.0

#echo "selected_spheres.sph">selected_spheres.in
#echo "1">>selected_spheres.in
#echo "N">>selected_spheres.in
#echo "selected_spheres.pdb">>selected_spheres.in

#$dock6bindir/showsphere < selected_spheres.in

endif

### Construct box to enclose spheres
echo "Y">box.in
echo "5">>box.in
echo "selected_spheres.sph">>box.in
echo "1">>box.in
echo "rec_box.pdb">>box.in
$dock6bindir/showbox < box.in

##Obliczenie gridu
if ( ! -s grid.bmp || ! -s grid.nrg )  then
## Compute scoring grids
echo "compute_grids                  yes"> grid.in
echo "grid_spacing                   $space">> grid.in
echo "output_molecule                no">> grid.in
echo "contact_score                  no">> grid.in
echo "energy_score                   yes">> grid.in
echo "energy_cutoff_distance         9999">> grid.in
echo "atom_model                     a">> grid.in
echo "attractive_exponent            6">> grid.in
echo "repulsive_exponent             12">> grid.in
echo "distance_dielectric            yes">> grid.in
echo "dielectric_factor              4">> grid.in
echo "bump_filter                    yes">> grid.in
echo "bump_overlap                   0.75">> grid.in
echo "receptor_file                  $rclean">> grid.in
echo "box_file                       rec_box.pdb">> grid.in
echo "vdw_definition_file            $dock6bindir/../parameters/vdw_AMBER_parm99.defn">> grid.in
echo "score_grid_prefix              grid">> grid.in
$dock6bindir/grid -i grid.in
endif


## Regenerate ligand:receptor complex with rigid ligand 
echo "ligand_atom_file                                             $lname.mol2">rigid.in
echo "limit_max_ligands                                            no">>rigid.in
echo "skip_molecule                                                no">>rigid.in
echo "read_mol_solvation                                           no">>rigid.in
echo "calculate_rmsd                                               no">>rigid.in
echo "use_database_filter                                          no">>rigid.in
echo "orient_ligand                                                yes">>rigid.in
echo "automated_matching                                           yes">>rigid.in
echo "receptor_site_file                                           selected_spheres.sph">>rigid.in
echo "max_orientations                                             1000">>rigid.in
echo "critical_points                                              no">>rigid.in
echo "chemical_matching                                            no">>rigid.in
echo "use_ligand_spheres                                           no">>rigid.in
echo "use_internal_energy                                          yes">>rigid.in
echo "internal_energy_rep_exp                                      12">>rigid.in
echo "flexible_ligand                                              no">>rigid.in
echo "bump_filter                                                  no">>rigid.in
echo "score_molecules                                              yes">>rigid.in
echo "contact_score_primary                                        no">>rigid.in
echo "contact_score_secondary                                      no">>rigid.in
echo "grid_score_primary                                           yes">>rigid.in
echo "grid_score_secondary                                         no">>rigid.in
echo "grid_score_rep_rad_scale                                     1">>rigid.in
echo "grid_score_vdw_scale                                         1">>rigid.in
echo "grid_score_es_scale                                          1">>rigid.in
echo "grid_score_grid_prefix                                       grid">>rigid.in
echo "multigrid_score_secondary                                    no">>rigid.in
echo "dock3.5_score_secondary                                      no">>rigid.in
echo "continuous_score_secondary                                   no">>rigid.in
echo "descriptor_score_secondary                                   no">>rigid.in
echo "gbsa_zou_score_secondary                                     no">>rigid.in
echo "gbsa_hawkins_score_secondary                                 no">>rigid.in
echo "SASA_descriptor_score_secondary                              no">>rigid.in
echo "amber_score_secondary                                        no">>rigid.in
echo "minimize_ligand                                              yes">>rigid.in
echo "simplex_max_iterations                                       1000">>rigid.in
echo "simplex_tors_premin_iterations                               0">>rigid.in
echo "simplex_max_cycles                                           1">>rigid.in
echo "simplex_score_converge                                       0.1">>rigid.in
echo "simplex_cycle_converge                                       1.0">>rigid.in
echo "simplex_trans_step                                           1.0">>rigid.in
echo "simplex_rot_step                                             0.1">>rigid.in
echo "simplex_tors_step                                            10.0">>rigid.in
echo "simplex_random_seed                                          0">>rigid.in
echo "simplex_restraint_min                                        no">>rigid.in
echo "atom_model                                                   all">>rigid.in
echo "vdw_defn_file                                                $dock6bindir/../parameters/vdw_AMBER_parm99.defn">>rigid.in
echo "flex_defn_file                                               $dock6bindir/../parameters/flex.defn">>rigid.in
echo "flex_drive_file                                              $dock6bindir/../parameters/flex_drive.tbl">>rigid.in
echo "ligand_outfile_prefix                                        rigid">>rigid.in
echo "write_orientations                                           no">>rigid.in
echo "num_scored_conformers                                        10">>rigid.in
echo "rank_ligands                                                 no">>rigid.in
echo "write_conformations                                          yes">>rigid.in
echo "cluster_conformations                                          yes">>rigid.in
echo "cluster_rmsd_threshold                                          2.0">>rigid.in
echo "max_ranked_ligands                                          50">>rigid.in

##Dokowanie
$dock6bindir/dock6 -i rigid.in

set results=rigid_scored.mol2

grep 'Name' $results >names
grep 'Score' $results >scores


##Postprocessing
obabel $results -Ores.sdf -m
set lname0=$lname
set k=1
set m=0
set line=`sed -n ${k}p names`
set tmp=`echo $line | tr ":" " "`
set lname=${tmp[3]}
mkdir -p $outd/$lname
foreach i ( `ls -1v res*.sdf` )
set line=`sed -n ${k}p names`
#echo $k $line `echo $line | tr ":" " "`
#set tmp=`split(/:/, $line )`
set tmp=`echo $line | tr ":" " "`
set oldlname=$lname
set lname=${tmp[3]}
#echo $oldlname  $lname
if ( $oldlname != $lname ) then
@ m = 0
mkdir -p $outd/$lname

endif
set out=$outd/$lname/$aname'_'$rname"_"$lname.sdf
set line=`sed -n ${k}p scores`
#set tmp=`split(/:/, $line )`
set tmp=`echo $line | tr ":" " "`
set score=${tmp[4]}
echo $aname $rname $lname $m $score>>$out
tail -n+2 $i >>$out
@ k = $k + 1
@ m = $m + 1
rm $i
end

rm rigid* anchor_and_grow* $rname* *.in rec*  tmp* $lname0.mol2 INSPH radii names scores

