#!/bin/tcsh

##Dekalracja zmiennych
$set_env_var
set rec=$1
set lig=$2
set outd=$3
set work=$4
set clip=$5

set lname=$lig:t:r
set rname=$rec:t:r


set rclean=$rname-clean.pdb

mkdir $work
cd $work
cp $lig $rec ./

##Przygotowanie receptora i ligandów
python $scripts/clean_pdb.py $rname.pdb tmp.pdb
#$obabel tmp.pdb -O$rclean
obabel tmp.pdb -Oprec.mol2 -h
obabel $lig -Otmplig.mol2
echo " $surflex  +misc_remin +fp +misc_ring -misc_outconfs 1  prot tmplig.mol2 plig">plik0
$sybyl < plik0
echo " $surflex -misc_outconfs 1 prot $rclean prec">plik0

##Przygotowanie pliku protomol zawierającego niezbędne inforamcje o miejscu wiążącym
if ( "$clip" == 'False' ) then
echo " $surflex -proto_thresh 0.50 -proto_bloat 0 proto none prec.mol2 p1;">plik1
else
echo " $surflex -proto_thresh 0.50 -proto_bloat 0 proto $clip prec.mol2 p1;">plik1
endif

##Polecenie dokujące
echo " $surflex -grid_bloat 6 -maxconfs 20 -maxrot 100 -self_score -ring -rigid +soft_box +premin +remin +frag +spinalign -spindense 3.00 -nspin 12 -ndock_final 5 -div_rms 0.5 dock_list plig.mol2 p1-protomol.mol2 prec.mol2 logi;" >plik2


##Wywołanie poleceń
if ( ! -s p1-protomol.mol2 ) then
if ( ! -s prec.mol2 ) then
$sybyl < plik0
endif

$sybyl < plik1
endif
if ( ! -s p1-protomol.mol2 ) then
echo " $surflex -proto_thresh 0.50 -proto_bloat 0 proto none prec.mol2 p1;">plik1
$sybyl < plik1
endif
$sybyl < plik2


#obróbka otrzymanych wyników
obabel -imol2 logi-results.mol2 -osdf -Ologi-results.sdf

python $scripts/surflex_to_sdf.py logi-results.sdf logi-results_tab.log $rname $outd
#rm log"_"$i-results.sdf


#head -n -1 tmp>tmp1
#echo -ne '$$$$' >> tmp1
#cat tmp1>$out

#czyszczenie
#rm tmp tmp1
rm plik* rec
rm *.mol2
rm p1-v* p0* log*

