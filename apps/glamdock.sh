#!/bin/tcsh

##Deklaracja podstawowych zmiennych
$set_env_var

set rec=$1
set ligs=$2
set outd=$3
set work=$4
set clip=$5




set lname=$ligs:t:r
set rname=$rec:t:r

mkdir -p $work
cd $work

if ( "$clip" == 'False' ) then
set grid=''
else
obabel $clip -Oclip.mol2 
set grid=clip.mol2
endif

set rclean=receptor.mol2
python $scripts/clean_pdb.py $rec tmp.pdb
obabel tmp.pdb -O$rclean -h
rm tmp.pdb

##Przygotowanie listy dokowań
set csv=`pwd`/$rname.csv
#set i=0
echo "protein\t""ligand\t""resID\t""reference">$csv
obabel $ligs -O tlig.mol2 -m 
foreach lig (`ls tlig*.mol2`)
set lname=`sed -n 2p $lig`
mv $lig $lname.mol2
echo "`realpath $rclean`	`realpath $lname.mol2`	$lname	`realpath clip.mol2`">>$csv#	
#@ i= $i + 1
end

#Dokowanie(Program wymaga uruchomienia z folderu instalacyjnego)
cd $chill_dir
glamdock -o io_input_file=$csv,io_output_dir=$work,post_cluster_rmsd=2.0,post_cluster_count=10
cd $work

set end=`cat poses.csv | wc -l`
set k=0

set tmp=(`sed -n 2p poses.csv`)
set lname=${tmp[9]:t:r}
mkdir -p $outd/$lname

set header=(`sed -n 1p poses.csv`)


##Postrocessing decoyów
foreach i (`seq 2 $end`)
set tmp=(`sed -n ${i}p poses.csv`)
set oldlname=$lname
set lname=${tmp[9]:t:r}
if ( $oldlname != $lname ) then
@ k = 0
mkdir -p $outd/$lname
endif

set out=$outd/$lname/glamdock'_'$rname"_"$lname.sdf
set res=RANK_${k}_lig.sdf
if ( ! -s $res ) then
obabel output_files/$lname/docked.mol2 -O$lname.sdf -m
foreach lig (`ls $lname*.sdf`)
set tname=`sed -n 1p $lig`
mv $lig $tname.sdf
end
endif
#echo $oldlname  $lname

set score=${tmp[8]}
echo glamdock $rname $lname $k $score

echo "glamdock $rname $lname $k $score">>$out
tail -n+2 $res|head -n-1 >>$out
echo "> <Log>" >> $out
echo ${header[*]} >> $out
echo ${tmp[*]} >> $out
echo '$$$$' >> $out
rm $res

@ k = $k + 1

end
rm -r *
