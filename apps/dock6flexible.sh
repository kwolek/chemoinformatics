#!/bin/tcsh

#Deklaracja podstawowych zmiennych
$set_env_var
set rec=$1
set ligs=$2
set outd=$3
set work=$4
set clip=$5
set aname=dock6flexible


set lname=$ligs:t:r
set rname=$rec:t:r
#set cname=$clip:t:r
mkdir -p $work
#cp *.in $work
cd $work

#Przygotowanie ligandów
set rclean=$rname'_'clean.pdb
obabel $ligs -O$lname.mol2

set cname=clip

if ( "$clip" == 'False' ) then
obabel $rec -O$cname.mol2 -h
set space=0.75
else
obabel $clip -O$cname.mol2 -h
set space=0.5
endif

#Przygotowanie miejsca wiążącego
if ( ! -s selected_spheres.sph )  then
if ( ! -s rec.sph ) then
if ( -e rec.sph ) then
rm rec.sph
endif
python $scripts/clean_pdb.py $rec tmp.pdb
#$reduce tmp.pdb > $rclean
obabel tmp.pdb -O$rclean -h


cp $dms/lib/radii.proto radii
$dms/dms $rclean -o rec.ms


# Construct sphere set in active site
echo "rec.ms" >INSPH
echo "R" >>INSPH
echo "X" >>INSPH
echo "0.0" >>INSPH
echo "4.0" >>INSPH
echo "1.4" >>INSPH
echo "rec.sph" >>INSPH

$dock6bindir/sphgen_cpp
#rm OUTSPH temp1.ms temp3.atc temp2.sph INSPH
endif



# Select spheres within 10 Ang of ligand 

$dock6bindir/sphere_selector rec.sph $cname.mol2 7.5

#echo "selected_spheres.sph">selected_spheres.in
#echo "1">>selected_spheres.in
#echo "N">>selected_spheres.in
#echo "selected_spheres.pdb">>selected_spheres.in

#$dock6bindir/showsphere < selected_spheres.in

endif

### Construct box to enclose spheres
echo "Y">box.in
echo "5">>box.in
echo "selected_spheres.sph">>box.in
echo "1">>box.in
echo "rec_box.pdb">>box.in
$dock6bindir/showbox < box.in

#Przygotowanie gridu do dokowania
if ( ! -s grid.bmp || ! -s grid.nrg )  then
## Compute scoring grids
echo "compute_grids                  yes"> grid.in
echo "grid_spacing                   $space">> grid.in
echo "output_molecule                no">> grid.in
echo "contact_score                  no">> grid.in
echo "energy_score                   yes">> grid.in
echo "energy_cutoff_distance         9999">> grid.in
echo "atom_model                     a">> grid.in
echo "attractive_exponent            6">> grid.in
echo "repulsive_exponent             12">> grid.in
echo "distance_dielectric            yes">> grid.in
echo "dielectric_factor              4">> grid.in
echo "bump_filter                    yes">> grid.in
echo "bump_overlap                   0.75">> grid.in
echo "receptor_file                  $rclean">> grid.in
echo "box_file                       rec_box.pdb">> grid.in
echo "vdw_definition_file            $dock6bindir/../parameters/vdw_AMBER_parm99.defn">> grid.in
echo "score_grid_prefix              grid">> grid.in
$dock6bindir/grid -i grid.in
endif


## Regenerate ligand:receptor complex with flex ligand
echo "ligand_atom_file                                             $lname.mol2">anchor_and_grow.in
echo "limit_max_ligands                                            no">>anchor_and_grow.in
echo "skip_molecule                                                no">>anchor_and_grow.in
echo "read_mol_solvation                                           no">>anchor_and_grow.in
echo "calculate_rmsd                                               no">>anchor_and_grow.in
echo "use_database_filter                                          no">>anchor_and_grow.in
echo "orient_ligand                                                yes">>anchor_and_grow.in
echo "automated_matching                                           yes">>anchor_and_grow.in
echo "receptor_site_file                                           selected_spheres.sph">>anchor_and_grow.in
echo "max_orientations                                             1000">>anchor_and_grow.in
echo "critical_points                                              no">>anchor_and_grow.in
echo "chemical_matching                                            no">>anchor_and_grow.in
echo "use_ligand_spheres                                           no">>anchor_and_grow.in
echo "use_internal_energy                                          yes">>anchor_and_grow.in
echo "internal_energy_rep_exp                                      12">>anchor_and_grow.in
echo "flexible_ligand                                              yes">>anchor_and_grow.in
echo "user_specified_anchor                                        no">>anchor_and_grow.in
echo "limit_max_anchors                                            no">>anchor_and_grow.in
echo "min_anchor_size                                              40">>anchor_and_grow.in
echo "pruning_use_clustering                                       yes">>anchor_and_grow.in
echo "pruning_max_orients                                          100">>anchor_and_grow.in
echo "pruning_clustering_cutoff                                    100">>anchor_and_grow.in
echo "pruning_conformer_score_cutoff                               25.0">>anchor_and_grow.in
echo "use_clash_overlap                                            no">>anchor_and_grow.in
echo "write_growth_tree                                            no">>anchor_and_grow.in
echo "bump_filter                                                  no">>anchor_and_grow.in
echo "score_molecules                                              yes">>anchor_and_grow.in
echo "contact_score_primary                                        no">>anchor_and_grow.in
echo "contact_score_secondary                                      no">>anchor_and_grow.in
echo "grid_score_primary                                           yes">>anchor_and_grow.in
echo "grid_score_secondary                                         no">>anchor_and_grow.in
echo "grid_score_rep_rad_scale                                     1">>anchor_and_grow.in
echo "grid_score_vdw_scale                                         1">>anchor_and_grow.in
echo "grid_score_es_scale                                          1">>anchor_and_grow.in
echo "grid_score_grid_prefix                                       grid">>anchor_and_grow.in
echo "multigrid_score_secondary                                    no">>anchor_and_grow.in
echo "dock3.5_score_secondary                                      no">>anchor_and_grow.in
echo "continuous_score_secondary                                   no">>anchor_and_grow.in
echo "descriptor_score_secondary                                   no">>anchor_and_grow.in
echo "gbsa_zou_score_secondary                                     no">>anchor_and_grow.in
echo "gbsa_hawkins_score_secondary                                 no">>anchor_and_grow.in
echo "SASA_descriptor_score_secondary                              no">>anchor_and_grow.in
echo "amber_score_secondary                                        no">>anchor_and_grow.in
echo "minimize_ligand                                              yes">>anchor_and_grow.in
echo "minimize_anchor                                              yes">>anchor_and_grow.in
echo "minimize_flexible_growth                                     yes">>anchor_and_grow.in
echo "use_advanced_simplex_parameters                              no">>anchor_and_grow.in
echo "simplex_max_cycles                                           1">>anchor_and_grow.in
echo "simplex_score_converge                                       0.1">>anchor_and_grow.in
echo "simplex_cycle_converge                                       1.0">>anchor_and_grow.in
echo "simplex_trans_step                                           1.0">>anchor_and_grow.in
echo "simplex_rot_step                                             0.1">>anchor_and_grow.in
echo "simplex_tors_step                                            10.0">>anchor_and_grow.in
echo "simplex_anchor_max_iterations                                5000">>anchor_and_grow.in
echo "simplex_grow_max_iterations                                  5000">>anchor_and_grow.in
echo "simplex_grow_tors_premin_iterations                          0">>anchor_and_grow.in
echo "simplex_random_seed                                          0">>anchor_and_grow.in
echo "simplex_restraint_min                                        no">>anchor_and_grow.in
echo "atom_model                                                   all">>anchor_and_grow.in
echo "vdw_defn_file                                                $dock6bindir/../parameters/vdw_AMBER_parm99.defn">>anchor_and_grow.in
echo "flex_defn_file                                               $dock6bindir/../parameters/flex.defn">>anchor_and_grow.in
echo "flex_drive_file                                              $dock6bindir/../parameters/flex_drive.tbl">>anchor_and_grow.in
echo "ligand_outfile_prefix                                        anchor_and_grow">>anchor_and_grow.in
echo "write_orientations                                           no">>anchor_and_grow.in
echo "num_scored_conformers                                        10">>anchor_and_grow.in
echo "rank_ligands                                                 no">>anchor_and_grow.in
echo "write_conformations                                          yes">>anchor_and_grow.in
echo "cluster_conformations                                          yes">>anchor_and_grow.in
echo "cluster_rmsd_threshold                                          1.0">>anchor_and_grow.in
echo "max_ranked_ligands                                          50">>anchor_and_grow.in


##Dokowanie
$dock6bindir/dock6 -i anchor_and_grow.in 

set results=anchor_and_grow_scored.mol2

grep 'Name' $results >names
grep 'Score' $results >scores


##Postprocessing
obabel $results -Ores.sdf -m
set lname0=$lname
set k=1
set m=0
set line=`sed -n ${k}p names`
set tmp=`echo $line | tr ":" " "`
set lname=${tmp[3]}
mkdir -p $outd/$lname
foreach i ( `ls -1v res*.sdf` )
set line=`sed -n ${k}p names`
#echo $k $line `echo $line | tr ":" " "`
#set tmp=`split(/:/, $line )`
set tmp=`echo $line | tr ":" " "`
set oldlname=$lname
set lname=${tmp[3]}
#echo $oldlname  $lname
if ( $oldlname != $lname ) then
@ m = 0
mkdir -p $outd/$lname

endif
set out=$outd/$lname/$aname'_'$rname"_"$lname.sdf
set line=`sed -n ${k}p scores`
#set tmp=`split(/:/, $line )`
set tmp=`echo $line | tr ":" " "`
set score=${tmp[4]}
echo $aname $rname $lname $m $score>>$out
tail -n+2 $i >>$out
@ k = $k + 1
@ m = $m + 1
rm $i
end

rm rigid* anchor_and_grow* $rname* *.in  rec*  tmp* $lname0.mol2 INSPH radii names scores

