#!/bin/tcsh

##Dekalracja podstawowych zmiennych
$set_env_var
set rec=$1
set ligs=$2
set outd=$3
set work=$4
set clip=$5


mkdir -p $work
cd $work

set rname=$rec:t:r
set rclean=$rname-clean.pdb

#Przygotowanie ligandów i miejsca wiążącego
obabel $ligs -O tlig.mol -m 
if ( "$clip" == 'False' ) then
python $scripts/clean_pdb.py $rec $rclean
else
obabel $clip -Otmp.pdb
set cav=`mod_cav $rec tmp.pdb 10 | cut -d' ' -f2`
python $scripts/clean_pdb.py $cav $rclean
rm tmp.pdb $cav
endif
mkdir -p ligands PrePDB/gemdock_out
foreach lig (`ls tlig*.mol`)
set lname=`sed -n 1p $lig`
mv $lig $lname.mol


##Dokowanie
gemdock 200 $rclean $lname.mol 6 1 1 1 2 0 60 5 1 0 0 

##Postprocessing
obabel `ls PrePDB/docked_Pose/*$lname*.pdb` -osdf -Ogemdock'_'$rname'_'$lname.tmp -d

obabel $lname.mol -O$lname.sdf -d
python $scripts/convert_to_sdf.py gemdock'_'$rname'_'$lname.tmp $outd $lname.sdf
rm gemdock'_'$rname'_'$lname.tmp $lname.sdf $lname.mol
end

