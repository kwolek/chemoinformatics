#!/bin/tcsh


##Deklaacja podstawowych zmiennych
$set_env_var
set rec=$1
set lig=$2
set outd=$3
set work=$4
set clip=$5


set lname=$lig:t:r
set rname=$rec:t:r
set tmp=tmp.sdf

set rclean=$rname-clean.pdb

cd $work
python $scripts/clean_pdb.py $rec tmp.pdb  
obabel tmp.pdb -O$rclean -h


set lclean=$lname.sdf
obabel $lig -O$lclean 

##Dokowanie
if ( "$clip" == 'False' ) then
ehits -ligand $lclean -receptor $rclean -out $tmp -workdir $work -select 10 -bindener
else
ehits -ligand $lclean -receptor $rclean -out $tmp -workdir $work -clip $clip -select 10 -accuracy 4 -bindener #-margin 10
endif


##Postprocessing
foreach i (`find results/$rname-clean/$lname/ -name $tmp`)
set bp=`dirname $i`/best_pose.mol
set lname=`head -n 1 $bp`
obabel $i -osdf -Oehits'_'$rname'_'$lname.tmp
python $scripts/convert_to_sdf.py ehits'_'$rname'_'$lname.tmp $outd
rm ehits'_'$rname'_'$lname.tmp
end
#rm -r results/$rname-clean/$lname/
rm -r preprocess/ligands
rm $tmp
rm tmp.pdb
