#!/bin/tcsh

##Deklaracja zmiennych
$set_env_var
set rec=$1
set ligs=$2
set outd=$3
set work=$4
set clip=$5


set lname=$ligs:t:r
set rname=$rec:t:r

set rclean=$rname'_'clean.pdb


mkdir $work
cd $work

#Deklaracja miejsca wiążącego
if ( "$clip" == 'False' ) then
set grid=(`$scripts/protein_size.py $rec sphere 1.2`)
else
obabel $clip -Otmp.pdb
set grid=(`$scripts/protein_size.py tmp.pdb sphere 1.2`)
endif

echo $grid
#python $scripts/clean_pdb.py $rec $rclean

##Przygotowanie receptora
obabel $rec -Otmp.pdb
spores --mode splitpdb tmp.pdb
rm tmp.pdb 

#$obabel $ligs -O$lname.mol2
obabel $ligs -Olname.mol2

#$spores --mode completemol2 $lname.mol2 lname.mol2


set intmp=tmp.in
# scoring function and search settings
echo "scoring_function chemplp">$intmp
echo "search_speed speed1">>$intmp
# input
echo "protein_file protein.mol2">>$intmp
echo "ligand_file lname.mol2">>$intmp
# output
echo "output_dir results">>$intmp
# write single mol2 files (e.g. for RMSD calculation)
echo "write_multi_mol2 0">>$intmp

# binding site definition
echo "bindingsite_center ${grid[1]}	${grid[2]}	${grid[3]}">>$intmp
echo "bindingsite_radius ${grid[4]}">>$intmp


# cluster algorithm
echo "cluster_structures 10">>$intmp
echo "cluster_rmsd 2.0">>$intmp

#mkdir -p old_results
#mv results/* old_results
rm results -r

##Dokowanie
plants --mode screen $intmp


##Postprocessing wyników
foreach i (`ls results/*.mol2 | grep -v \protein`)

set t=(`cat results/ligand.log | grep $i:t:r`)
set tmp=(`echo ${t[6]}|tr "_" " "`)
set lname=${tmp[1]}
set out=$outd/$lname/plants"_"$rname"_"$lname.sdf
mkdir -p $outd/$lname/
echo plants $rname $lname ${t[2]} ${t[4]} >> $out
obabel $i -osdf -Otmp1
tail -n+2 tmp1  >> $out

end
rm *mol2 -r *results *pid tmp*
