#!/bin/tcsh



#Deklaracja zmiennych
$set_env_var
echo $set_env_var
echo $scripts
set rec=$1
set ligs=$2
set outd=$3
set work=$4
set clip=$5


set rname=$rec:t:r
set rclean=$rname-clean
set tlig=tmplig.sdf

#Przygotowanie miejsca pracy
mkdir $work
cd $work

#Przygotowanie receptora
python $scripts/clean_pdb.py $rec $rclean.pdb  
python $adt/prepare_receptor4.py -r $rclean.pdb -A hydrogens

obabel $ligs -Otlig.sdf -m
foreach lig (`ls tlig*.sdf`)

#Przygotowanie ligandu
set lname=`head -n 1 $lig`
obabel -isdf $lig -o sdf -O $tlig -d
obabel -i sdf $lig -o pdb -O $lname.pdb 
timeout 20 python $adt/prepare_ligand4.py -l $lname.pdb -Z -A hydrogens -v

#Przygotowanie parametrów

if ( "$clip" == 'False' ) then
python $adt/prepare_gpf4.py -r ${rclean}.pdbqt -l "$lname.pdbqt" -o $rname.gpf
else
set spacing=0.25
set margin=5
set grid=(`$scripts/protein_size.py $clip autodock $spacing $margin`)
python $adt/prepare_gpf4.py -r ${rclean}.pdbqt -l "$lname.pdbqt" -o tmp.gpf
sed "s/npts .*  #/npts  ${grid[4]} ${grid[5]} ${grid[6]}  #/g" tmp.gpf |sed "s/spacing .* #/spacing $spacing  #/g"|sed "s/gridcenter .*  #/gridcenter ${grid[1]} ${grid[2]} ${grid[3]}  #/g">$rname.gpf

endif


python $adt/prepare_dpf4.py -r ${rclean}.pdbqt -l "$lname.pdbqt" -o $rname"_"$lname.dpf

#Dokowanie
autogrid4 -p $rname.gpf -l $rname.glg
autodock4 -p $rname"_"$lname.dpf -l autodock4'_'$rname"_"$lname.dlg

#Obróbka i skrawanie
python $scripts/dlg_to_pdbqt.py autodock4'_'$rname"_"$lname.dlg $lname.tmp
obabel -ipdb $lname.tmp -osdf -Oautodock4'_'$rname"_"$lname.tmp -d
python $scripts/convert_to_sdf.py autodock4'_'$rname"_"$lname.tmp $outd $tlig

#Czyszczenie
rm $lname.* $lig
rm *.tmp
end
#rm $rname*
#rm autodock4*


