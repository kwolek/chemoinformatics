#!/bin/tcsh

##Deklaracja zmiennych
set rec=$1
set ligs=$2
set outd=$3
set work=$4
set clip=$5

$set_env_var

#setenv SCHRODINGER /opt/schrodinger


module load schrodinger/2012

set start=`pwd`
set lname=$ligs:t:r
set rname=$rec:t:r

set lig=$lname.sdf

mkdir $work
cd $work

	$SCHRODINGER/ligprep  -vary_tc -WAIT -isd $ligs -osd $lig

	# tu wywalam smieci z pliku pdb 
	#obabel $lig -O$lig2 -h
	if ( ! -e "$rname'_'maestro.mae" ) then
	python $scripts/clean_pdb.py $rec $rname'_'clean.pdb
	
	# tu umieszczamy konwersje pdb do mae
	
	$SCHRODINGER/utilities/pdbconvert -ipdb $rname'_'clean.pdb -omae $rname'_'clean.mae

	# tu puszczamy prep_wizard
	echo Czyszcze bialko

	$SCHRODINGER/utilities/prepwizard -WAIT -rehtreat -noimpref $rname'_'clean.mae $rname'_'maestro.mae
	endif
	#generowanie inputu do glide_grid
	
	python $scripts/make_glide_input.py  glide-grid.in glide-dock.in $clip 15 15 $rname'_'maestro.mae $rname'_'maestro $lig glide.$rname.grid.zip $rname'_'clean.pdb
	
	#scripts/generator_inputu_glide.pl $lig glide-grid.in 10 glide-$rname-grid.zip ${rname}_maestro.mae ${rname}_maestro
	#scripts/generator_inputu_glide_czesc_2.pl glide-dock.in $lig

	#puszczanie generowania grid-a
	if ( ! -e glide.$rname.grid.zip ) then
	echo Generuje grid
	$SCHRODINGER/glide -WAIT glide-grid.in
	endif
	#puszczamy dokowanie
	echo Dokuje ligandy	
	$SCHRODINGER/glide -WAIT glide-dock.in
	#Obrabiamy wyniki jesli zostały wyrzucone
	if (  -e glide-dock_lib.sdfgz ) then
	mv glide-dock_lib.sdfgz glide'_'$rname'_'$lname.sdf.gz
	gunzip -f glide'_'$rname'_'$lname.sdf.gz
	python $scripts/convert_to_sdf.py glide'_'$rname'_'$lname.sdf $outd
	rm glide'_'$rname'_'$lname.sdf
	else 
	echo "Dokowanie się nie udało"
	endif
	#czyścimy
	rm glide-* *clean*
	#rm $rname*
	


