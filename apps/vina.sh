#!/bin/tcsh
##Deklaracja podstawowych zmiennych
$set_env_var
set rec=$1
set ligs=$2
set outd=$3
set work=$4
set clip=$5

set lname=$ligs:t:r
set rname=$rec:t:r
set rclean=$rname-clean
set tlig=tmplig.sdf
mkdir $work
cd $work

##Deklaracja miejsca wiążącego
if ( "$clip" == 'False' ) then
set grid=(`$scripts/protein_size.py $rec vina 5 5 5`)
else
obabel $clip -Otmp.pdb
set grid=(`$scripts/protein_size.py tmp.pdb vina 7 7 7`)
endif

python $scripts/clean_pdb.py $rec $rclean.pdb
python $adt/prepare_receptor4.py -r $rclean.pdb -A hydrogens 


obabel $ligs -Otlig.sdf -m
foreach lig (`ls tlig*.sdf`)
set lname=`head -n 1 $lig`
echo $lname
#set out=$outd/$lname/autodock4'_'$rname'_'$lname.sdf



##Przygotowanie ligandu(czasem nie chce się skończyć, zwykle trwa poniżej sekundy, dlatego timeout)
obabel -isdf $lig -osdf -O$tlig -d
obabel -isdf $lig -opdb -O$lname.pdb 
date
timeout 200 python $adt/prepare_ligand4.py -l $lname.pdb -Z -A hydrogens 
date
ls *.pdbqt -l
##Dokowanie(pamięciożerne przy dużych ligandach i bialkach)
#$scripts/timeout -m 7000000 "vina --receptor $rclean.pdbqt --ligand $lname.pdbqt $grid --out $lname  --cpu 8  --num_modes 10" #--energy_range 30 --exhaustiveness 10
vina --receptor $rclean.pdbqt --ligand $lname.pdbqt $grid --out $lname  --num_modes 10 #--exhaustiveness 20 --energy_range 30 

##Obróbka wyników
obabel -ipdbqt $lname -osdf -Ovina_$rname'_'$lname.tmp -d
python $scripts/convert_to_sdf.py vina'_'$rname'_'$lname.tmp $outd $tlig

rm vina_$rname'_'$lname.tmp

rm $lname* $lig
end
rm tmp
#rm $rname.pdbqt
